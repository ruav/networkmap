package nwMap;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import nwMap.model.*;
import nwMap.view.*;
import org.controlsfx.dialog.Dialogs;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private ObservableList<NetworkItem> networkItems = FXCollections.observableArrayList();
	/**
	 * The data as an observable list of Persons.
	 */
	private ObservableList<Person> personData = FXCollections.observableArrayList();
	private ObservableList<Source> sourceData = FXCollections.observableArrayList();

	/**
	 * Constructor
	 */
	public MainApp() {
		// Add some sample data
//		personData.add(new Person("Hans", "Muster"));
//		personData.add(new Person("Ruth", "Mueller"));
//		personData.add(new Person("Heinz", "Kurz"));
//		personData.add(new Person("Cornelia", "Meier"));
//		personData.add(new Person("Werner", "Meyer"));
//		personData.add(new Person("Lydia", "Kunz"));
//		personData.add(new Person("Anna", "Best"));
//		personData.add(new Person("Stefan", "Meier"));
//		personData.add(new Person("Martin", "Mueller"));

		//source init
		{
			sourceData.add(new Source("tech"));
			sourceData.add(new Source("Telemech"));
			sourceData.add(new Source("electro"));
			sourceData.add(new Source("amq1"));
			sourceData.get(3).setType(SourceType.InfluxDB);
			sourceData.add(new Source("amq2"));
			sourceData.get(4).setType(SourceType.InfluxDB);
			sourceData.add(new Source("db"));
			sourceData.get(5).setType(SourceType.InfluxDB);
		}

		//network init
		{
			networkItems.add(new NetworkItem(new SimpleStringProperty("192.168.0.10"), new SimpleStringProperty("OPC1")));
			networkItems.get(0).getSourceListGUID().add(sourceData.get(0).getGuid());
			sourceData.get(0).setParent(networkItems.get(0).getGuid());
			networkItems.add(new NetworkItem(new SimpleStringProperty("192.168.0.11"), new SimpleStringProperty("OPC2")));
			networkItems.get(1).getSourceListGUID().add(sourceData.get(1).getGuid());
			sourceData.get(1).setParent(networkItems.get(1).getGuid());
//		System.out.println("networkItems guid = " + networkItems.get(1).getGuid() + " list guid" + networkItems.get(1).getSourceListGUID());
			networkItems.add(new NetworkItem(new SimpleStringProperty("10.10.10.21"), new SimpleStringProperty("Data srv")));
			networkItems.get(2).getSourceListGUID().add(sourceData.get(3).getGuid());
			networkItems.get(2).getSourceListGUID().add(sourceData.get(4).getGuid());
			networkItems.add(new NetworkItem(new SimpleStringProperty("10.10.10.10"), new SimpleStringProperty("DB srv")));
			networkItems.get(3).getSourceListGUID().add(sourceData.get(5).getGuid());
			sourceData.get(3).setParent(networkItems.get(2).getGuid());
			sourceData.get(4).setParent(networkItems.get(2).getGuid());
			sourceData.get(5).setParent(networkItems.get(3).getGuid());
		}

//		System.out.println(sourceData.get(3).getGuid());
//		System.out.println(sourceData.get(0).getConnectionGuid());
//		sourceData.get(0).getConnectionGuid().put("1","from");
		createConnectionBeetweenSources(sourceData.get(0),sourceData.get(3));


//		createConnectionBeetweenSources(sourceData.get(1),sourceData.get(4));
//		createConnectionBeetweenSources(sourceData.get(5),sourceData.get(4));
//		sourceData.get(0).getConnectionGuid().put(sourceData.get(3).getGuid(),"from");
//		sourceData.get(1).getConnectionGuid().put(sourceData.get(4).getGuid(),"from");
//		sourceData.get(5).getConnectionGuid().put(sourceData.get(4).getGuid(),"from");
		System.out.println("networkItems guid = " + networkItems.get(2).getGuid() + " list guid" + networkItems.get(2).getSourceListGUID());

	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("AddressApp");

		// Set the application icon.
		this.primaryStage.getIcons().add(
				new Image("file:resources/images/address_book_32.png"));

		initRootLayout();

//		showPersonOverview();
		showSourceOverview();
	}

	/**
	 * Initializes the root layout and tries to load the last opened
	 * person file.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			// Give the controller access to the main app.
			RootLayoutController controller = loader.getController();
			controller.setMainApp(this);

			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Try to load last opened person file.

//		File file = getSourceFilePath();
//		if (file != null) {
//			loadSourceDataFromFile(file);
//		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	public void showPersonOverview() {
		try {
			// Load person overview.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/PersonOverview.fxml"));
			AnchorPane personOverview = (AnchorPane) loader.load();

			// Set person overview into the center of root layout.
			rootLayout.setCenter(personOverview);

			// Give the controller access to the main app.
			PersonOverviewController controller = loader.getController();
			controller.setMainApp(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the source overview inside the root layout.
	 */
	public void showSourceOverview() {
		try {
			// Load source overview.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/SourceOverview.fxml"));
			AnchorPane sourceOverview = (AnchorPane) loader.load();

			// Set person overview into the center of root layout.
			rootLayout.setCenter(sourceOverview);

			// Give the controller access to the main app.
			SourceOverviewController controller = loader.getController();
			controller.setMainApp(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the map overview inside the root layout.
	 */
	public void showMapOverview() {
		try {
			// Load source overview.
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MapOverview.fxml"));

			Parent root1 = (Parent) loader.load();


			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Network map");
			stage.setScene(new Scene(root1));


			stage.show();
//
//			FXMLLoader loader = new FXMLLoader();
//			loader.setLocation(MainApp.class.getResource("view/MapOverview.fxml"));
//			AnchorPane sourceOverview = (AnchorPane) loader.load();
//
//			// Set person overview into the center of root layout.
//			rootLayout.setCenter(sourceOverview);
//
			// Give the controller access to the main app.
			MapOverviewController controller = loader.getController();
			controller.setMainApp(this);
			controller.loadData();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opens a dialog to edit details for the specified person. If the user
	 * clicks OK, the changes are saved into the provided person object and true
	 * is returned.
	 *
	 * @param person
	 *            the person object to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	public boolean showPersonEditDialog(Person person) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/PersonEditDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Person");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the person into the controller.
			PersonEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setPerson(person);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Opens a dialog to edit details for the specified source. If the user
	 * clicks OK, the changes are saved into the provided source object and true
	 * is returned.
	 *
	 * @param source
	 *            the person object to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	public boolean showSourceEditDialog(Source source) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/SourceEditDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Source");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the person into the controller.
			SourceEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setSource(source);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Opens a dialog to show birthday statistics.
	 */
	public void showBirthdayStatistics() {
		try {
			// Load the fxml file and create a new stage for the popup.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/BirthdayStatistics.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Birthday Statistics");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("file:resources/images/calendar.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the persons into the controller.
			BirthdayStatisticsController controller = loader.getController();
			controller.setPersonData(personData);

			dialogStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the person file preference, i.e. the file that was last opened.
	 * The preference is read from the OS specific registry. If no such
	 * preference can be found, null is returned.
	 * 
	 * @return
	 */
	public File getPersonFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		String filePath = prefs.get("filePath", null);
		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}

	/**
	 * Returns the person file preference, i.e. the file that was last opened.
	 * The preference is read from the OS specific registry. If no such
	 * preference can be found, null is returned.
	 *
	 * @return
	 */
	public File getSourceFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		String filePath = prefs.get("filePath", null);
		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}

	/**
	 * Sets the file path of the currently loaded file. The path is persisted in
	 * the OS specific registry.
	 *
	 * @param file the file or null to remove the path
	 */
	public void setPersonFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		if (file != null) {
			prefs.put("filePath", file.getPath());

			// Update the stage title.
			primaryStage.setTitle("AddressApp - " + file.getName());
		} else {
			prefs.remove("filePath");

			// Update the stage title.
			primaryStage.setTitle("AddressApp");
		}
	}

	/**
	 * Sets the file path of the currently loaded file. The path is persisted in
	 * the OS specific registry.
	 * 
	 * @param file the file or null to remove the path
	 */
	public void setSourceFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		if (file != null) {
			prefs.put("filePath", file.getPath());

			// Update the stage title.
			primaryStage.setTitle("AddressApp - " + file.getName());
		} else {
			prefs.remove("filePath");

			// Update the stage title.
			primaryStage.setTitle("AddressApp");
		}
	}


	/**
	 * Loads source data from the specified file. The current person data will
	 * be replaced.
	 *
	 * @param file
	 */
	public void loadSourceDataFromFile(File file) {
		try {
			JAXBContext context = JAXBContext
					.newInstance(SourceListWrapper.class);
			Unmarshaller um = context.createUnmarshaller();

			// Reading XML from the file and unmarshalling.
			SourceListWrapper wrapper = (SourceListWrapper) um.unmarshal(file);

			sourceData.clear();
			sourceData.addAll(wrapper.getSources());

			// Save the file path to the registry.
			setSourceFilePath(file);

		} catch (Exception e) { // catches ANY exception
//			Dialogs.create()
//					.title("Error")
//					.masthead("Could not load data from file:\n" + file.getPath())
//					.showException(e);
		}
	}



	/**
	 * Saves the current source data to the specified file.
	 *
	 * @param file
	 */
	public void saveSourceDataToFile(File file) {
		try {
			JAXBContext context = JAXBContext
					.newInstance(SourceListWrapper.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// Wrapping our person data.
			SourceListWrapper wrapper = new SourceListWrapper();
			wrapper.setSources(sourceData);

			// Marshalling and saving XML to the file.
			m.marshal(wrapper, file);

			// Save the file path to the registry.
			setSourceFilePath(file);
		} catch (Exception e) { // catches ANY exception
			Dialogs.create().title("Error")
					.masthead("Could not save data to file:\n" + file.getPath())
					.showException(e);
		}
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Returns the data as an observable list of Persons.
	 * 
	 * @return
	 */
	public ObservableList<Person> getPersonData() {
		return personData;
	}

	public ObservableList<Source> getSourceData() {
		return sourceData;
	}

	public ObservableList<NetworkItem> getNetworkItems() {
		return networkItems;
	}

	public void setNetworkItems(ObservableList<NetworkItem> networkItems) {
		this.networkItems = networkItems;
	}

	public Source getSourceByGuid(String guid){
		Source src = null;
		for(Source source : sourceData){
			if(source.getGuid().equals(guid)){
				src = source;
			}
		}
		return src;
	}

	public void createConnectionBeetweenSources(Source sourceFrom, Source sourceTo){
		sourceFrom.getConnectionGuid().put(sourceTo.getGuid(),"to");
		sourceTo.getConnectionGuid().put(sourceFrom.getGuid(),"from");
	}

	public void deleteAllConnectionFromSource(Source source){
		for(Source src :sourceData){
			src.getConnectionGuid().remove(source.getGuid());
		}
	}

	public void deleteConnectionBeetweenSources(Source source1, Source source2){
		source1.getConnectionGuid().remove(source2.getGuid());
		source2.getConnectionGuid().remove(source1.getGuid());
	}

	public static void main(String[] args) {
		launch(args);

	}
}