package nwMap.model;

/**
 * Created by RuAV on 07.02.2016.
 */
public enum SourceType {
    OPC, AMQ, InfluxDB, Redis;

    @Override
    public String toString() {
        if(this == OPC){
            return "OPC";
        }else if(this == AMQ){
            return "AMQ";
        }else if(this == InfluxDB){
            return "InfluxDB";
        }else if(this == Redis){
            return "Redis";
        }else {
            return "SourceType{}";
        }
    }
}
