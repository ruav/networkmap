package nwMap.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.util.HashMap;
import java.util.UUID;


/**
 * Created by RuAV on 07.02.2016.
 */
public class Source {
    private  StringProperty name;
    private  StringProperty ip;
    private  StringProperty progId;
    private  ObjectProperty<SourceType> type;
    private  StringProperty dbName;
    private  StringProperty login;
    private  StringProperty password;
    private  StringProperty keyPrefix;
    private  StringProperty guid;
    private  StringProperty parent;
    private  MapProperty<String, String> connectionGuid; //<GUID, ARROW>
//    private  StringProperty arrow;



    public Source(){
        this(new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""),
                new SimpleObjectProperty<SourceType>(SourceType.OPC), new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""),
                new SimpleStringProperty(""), new SimpleMapProperty<String, String>(FXCollections.observableMap(new HashMap<String, String>())));
    }

    public Source(String name) {
        this(new SimpleStringProperty(name), new SimpleStringProperty(""), new SimpleStringProperty(""),
                new SimpleObjectProperty<SourceType>(SourceType.OPC), new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleStringProperty(""),new SimpleStringProperty(""), new SimpleMapProperty<String, String>(FXCollections.observableMap(new HashMap<String, String>())));
    }

//    public Source(StringProperty name, StringProperty ip, StringProperty progId, ObjectProperty<SourceType> type, StringProperty dbName, StringProperty login, StringProperty password, StringProperty keyPrefix, StringProperty guid, ListProperty<String> connectionList) {
//        this.name = name;
//        this.ip = ip;
//        this.progId = progId;
//        this.type = type;
//        this.dbName = dbName;
//        this.login = login;
//        this.password = password;
//        this.keyPrefix = keyPrefix;
//        this.guid = guid;
//        this.connectionList = connectionList;
//    }

    public Source(StringProperty name, StringProperty ip, StringProperty progId, ObjectProperty<SourceType> type, StringProperty dbName, StringProperty login, StringProperty password, StringProperty keyPrefix, StringProperty parent, MapProperty<String,String> connectionGuid) {
        this.name = name;
        this.ip = ip;
        this.progId = progId;
        this.type = type;
        this.dbName = dbName;
        this.login = login;
        this.password = password;
        this.keyPrefix = keyPrefix;
        this.guid = new SimpleStringProperty (UUID.randomUUID().toString());
        this.parent = parent;
        this.connectionGuid = connectionGuid;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public String getIp() {
        return ip.get();
    }

    public StringProperty ipProperty() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = new SimpleStringProperty(ip);
    }

    public String getProgId() {
        return progId.get();
    }

    public StringProperty progIdProperty() {
        return progId;
    }

    public void setProgId(String progId) {
        this.progId = new SimpleStringProperty(progId);
    }

    public Object getType() {
        return type.get();
    }

    public ObjectProperty<SourceType> typeProperty() {
        return type;
    }

    public void setType(SourceType type) {
        this.type = new SimpleObjectProperty<SourceType>(type);
    }

    public String getDbName() {
        return dbName.get();
    }

    public StringProperty dbNameProperty() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = new SimpleStringProperty(dbName);
    }

    public String getLogin() {
        return login.get();
    }

    public StringProperty loginProperty() {
        return login;
    }

    public void setLogin(String login) {
        this.login = new SimpleStringProperty(login);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new SimpleStringProperty(password);
    }

    public String getKeyPrefix() {
        return keyPrefix.get();
    }

    public StringProperty keyPrefixProperty() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = new SimpleStringProperty(keyPrefix);
    }

    public String getGuid() {
        return guid.get();
    }

    public StringProperty guidProperty() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid.set(guid);
    }

    public ObservableMap<String, String> getConnectionGuid() {
        return connectionGuid.get();
    }

    public MapProperty<String, String> connectionGuidProperty() {
        return connectionGuid;
    }

    public void setConnectionGuid(ObservableMap<String, String> connectionGuid) {
        this.connectionGuid.set(connectionGuid);
    }

    public String getParent() {
        return parent.get();
    }

    public StringProperty parentProperty() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = new SimpleStringProperty(parent);
//        this.parent.set();
    }

    @Override
    public String toString() {
        return "Source{" +
                "name=" + name +
                ", ip=" + ip +
                ", progId=" + progId +
                ", type=" + type +
                ", dbName=" + dbName +
                ", login=" + login +
                ", password=" + password +
                ", keyPrefix=" + keyPrefix +
                ", guid=" + guid +
                ", parent=" + parent +
                '}';
    }
}
