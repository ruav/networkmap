package nwMap.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.*;

/**
 * Created by asutp on 20.02.2016.
 */
public class NetworkItem {
    private StringProperty ip;
    private StringProperty name;
    private StringProperty guid;
    private ListProperty<String> sourceListGUID;
    private MapProperty<String,ListProperty<String>> mapSources;
    private ListProperty<ConnectionInNetwork> connectionList;

    public NetworkItem() {
        this(new SimpleStringProperty(""), new SimpleStringProperty(""), new SimpleListProperty<String>(FXCollections.observableList(new ArrayList<String>())),
                new SimpleListProperty<ConnectionInNetwork>(FXCollections.observableList(new ArrayList<ConnectionInNetwork>())));
//        this.setIp("");
//        this.setSourceListGUID((ObservableList<Source>) new ArrayList<Source>());
    }

    public NetworkItem(StringProperty ip) {
        this(ip, new SimpleStringProperty(""),new SimpleListProperty<String>(FXCollections.observableList(new ArrayList<String>())),
                new SimpleListProperty<ConnectionInNetwork>(FXCollections.observableList(new ArrayList<ConnectionInNetwork>())));
    }
    public NetworkItem(String ip) {
        this(new SimpleStringProperty(ip), new SimpleStringProperty(""),new SimpleListProperty<String>(FXCollections.observableList(new ArrayList<String>())),
                new SimpleListProperty<ConnectionInNetwork>(FXCollections.observableList(new ArrayList<ConnectionInNetwork>())));
    }
    public NetworkItem(String ip, String name) {
        this(new SimpleStringProperty(ip), new SimpleStringProperty(name), new SimpleListProperty<String>(FXCollections.observableList(new ArrayList<String>())),
                new SimpleListProperty<ConnectionInNetwork>(FXCollections.observableList(new ArrayList<ConnectionInNetwork>())));
    }
    public NetworkItem(StringProperty ip, StringProperty name) {
        this(ip, name, new SimpleListProperty<String>(FXCollections.observableList(new ArrayList<String>())),
                new SimpleListProperty<ConnectionInNetwork>(FXCollections.observableList(new ArrayList<ConnectionInNetwork>())));
    }

    public NetworkItem(StringProperty ip, StringProperty name, ListProperty<String> sourceListGUID, ListProperty<ConnectionInNetwork> connectionList) {
        this.ip = ip;
        this.name = name;
        this.sourceListGUID = sourceListGUID;
        this.guid = new SimpleStringProperty (UUID.randomUUID().toString());
        this.mapSources = new SimpleMapProperty<>(FXCollections.observableMap(new HashMap<>()));
        this.connectionList = connectionList;
    }

    public String getIp() {
        return ip.get();
    }

    public StringProperty ipProperty() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip.set(ip);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public ObservableList<String> getSourceListGUID() {
        return sourceListGUID.get();
    }

    public ListProperty<String> sourceListGUIDProperty() {
        return sourceListGUID;
    }

    public void setSourceListGUID(ObservableList<String> sourceListGUID) {
        this.sourceListGUID.set(sourceListGUID);
    }

    public String getGuid() {
        return guid.get();
    }

    public StringProperty guidProperty() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid.set(guid);
    }

    public ObservableMap<String, ListProperty<String>> getMapSources() {
        return mapSources.get();
    }

    public MapProperty<String, ListProperty<String>> mapSourcesProperty() {
        return mapSources;
    }

    public void setMapSources(ObservableMap<String, ListProperty<String>> mapSources) {
        this.mapSources.set(mapSources);
    }



    public class ConnectionInNetwork {
        private ConnectionArrow connectionArrow;
        private String fromGUID;
        private String toGUID;

        public ConnectionInNetwork(ConnectionArrow connectionArrow, String fromGUID, String toGUID) {
            this.connectionArrow = connectionArrow;
            this.fromGUID = fromGUID;
            this.toGUID = toGUID;
        }

        public ConnectionArrow getConnectionArrow() {
            return connectionArrow;
        }

        public void setConnectionArrow(ConnectionArrow connectionArrow) {
            this.connectionArrow = connectionArrow;
        }

        public String getFromGUID() {
            return fromGUID;
        }

        public void setFromGUID(String fromGUID) {
            this.fromGUID = fromGUID;
        }

        public String getToGUID() {
            return toGUID;
        }

        public void setToGUID(String toGUID) {
            this.toGUID = toGUID;
        }
    }


    public enum ConnectionArrow{
        From, To;
    }
}