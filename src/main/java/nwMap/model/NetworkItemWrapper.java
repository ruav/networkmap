package nwMap.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by asutp on 20.02.2016.
 */
@XmlRootElement(name = "networkItems")
public class NetworkItemWrapper {
    private List<NetworkItem> networkItems;

    @XmlElement(name = "networkItems")
    public List<NetworkItem> getNetworkItems() {
        return networkItems;
    }

    public void setSources(List<NetworkItem> networkItems) {
        this.networkItems = networkItems;
    }
}
