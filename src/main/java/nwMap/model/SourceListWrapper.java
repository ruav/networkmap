package nwMap.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by RuAV on 07.02.2016.
 */
@XmlRootElement(name = "sources")
public class SourceListWrapper {
    private List<Source> sources;

    @XmlElement(name = "source")
    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }
}
