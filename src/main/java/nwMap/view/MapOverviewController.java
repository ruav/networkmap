package nwMap.view;

import nwMap.MainApp;
import nwMap.model.NetworkItem;
import nwMap.model.Source;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;


import java.util.*;

public class MapOverviewController {

	private List<Source> sourceTable = new ArrayList<>();

    @FXML
    private WebView webView;

	private WebEngine webEngine;
//    @FXML
//    private TableColumn<Person, String> lastNameColumn;

    @FXML
    private Label nameLabel;
    @FXML
    private Label ipLabel;
    @FXML
    private Label typeLabel;
    @FXML
    private Label progIDLabel;
    @FXML
    private Label dbNameLabel;
    @FXML
    private Label loginLabel;
	@FXML
    private Label passwordLabel;
	@FXML
	private Label dbNameSource;
	@FXML
	private TableView<Source> tableView;
	@FXML
	private TableColumn<Source, String> nameColumn;

	// Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public MapOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	// Initialize the source table with the two columns.

//		this.webEngine = this.webView.getEngine();

//		webView = new WebView();
//		WebEngine webEngine =  new WebEngine("https://yandex.ru");
//		WebEngine webEngine = webView.getEngine();
//		this.webEngine.load("http://yandex.ru");
		webEngine = this.webView.getEngine();
//		webEngine.load("http://yandex.ru");
		String path = System.getProperty("user.dir");
		System.out.println(path);
		path.replace("\\\\", "/");
		path +=  "/resources/local.html";
		webEngine.load("file:///" + path);
//		webEngine.loadContent("<html><body><b>JavaFX</b></body></html>");
//		webEngine.lo

//		nameColumn.setCellValueFactory(
//				cellData -> cellData.getValue().nameProperty());
        // Clear source details.
        showSourceDetails(null);

        // Listen for selection changes and show the source details when changed.
//		tableView.getSelectionModel().selectedItemProperty().addListener(
//				(observable, oldValue, newValue) -> showSourceDetails(newValue)
//		);
//		sourceTable.getSelectionModel().selectedItemProperty().addListener(
//				(observable, oldValue, newValue) -> showSourceDetails(newValue));

		JSObject jsobj = (JSObject) webEngine.executeScript("window");
		jsobj.setMember("java", new Bridge());

		System.out.println("data is load");
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
//        sourceTable.addAll(mainApp.getSourceData());
    }
    
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     * 
     * @param source the person or null
     */
    private void showSourceDetails(Source source) {
		if (source != null) {
    		// Fill the labels with info from the person object.
    		nameLabel.setText(source.getName());
			ipLabel.setText(source.getIp());
    	} else {
    		// source is null, remove all the text.
			nameLabel.setText("");
			ipLabel.setText("");
    	}
    }


	public void loadData(){
//		NetworkItem networkItem = new NetworkItem();
//		networkItem.setIp("192.168.0.10");
//		mainApp.getSourceData();
//		networkItem.setSourceListGUID(mainApp.getSourceData());
//		mainApp.getNetworkItems().add(networkItem);


//		if(mainApp.getNetworkItems().get(0).getSourceListGUID().size() > 0) {
//			tableView.setItems(mainApp.getNetworkItems().get(0).getSourceListGUID());
//		} else {
//			tableView.setItems(mainApp.getSourceData());
//		}

		
//		tableView.setId("MapOverview");
//		tableView.setStyle("-fx-background-color: white");
//		tableView.setItems(mainApp.getSourceData());
//		tableView.
//		for(Source s: tableView.getItems()){
//			System.out.println(s.getName() + " " + s.getType() + " " + s.nameProperty());
////			nameColumn.setCellFactory(ta);
//		}

//		System.out.println(mainApp.getNetworkItems().get(0).getSourceListGUID());
//		nameColumn.cellFactoryProperty().

//		nameColumn.setCellValueFactory(
//				cellData -> cellData.getValue().nameProperty());

//		ObservableList<String> temp = FXCollections.observableList(new ArrayList<String>());
//		temp.add(mainApp.getSourceData().get(0).getGuid());
//		temp.add(mainApp.getSourceData().get(2).getGuid());

//		mainApp.getNetworkItems().get(0).setSourceListGUID(temp);
//		System.out.println("E1 = " + mainApp.getNetworkItems().get(0).getName());
//		System.out.println("nI e1 sGUID sz = " + mainApp.getNetworkItems().get(0).getSourceListGUID().size());

//		nameColumn.setStyle("-fx-background-color: white");
//		nameColumn.setStyle("-fx-background-color: gray");



	}

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteSource() {
//		int selectedIndex = sourceTable.getSelectionModel().getSelectedIndex();
//		if (selectedIndex >= 0) {
//			sourceTable.remove(selectedIndex);
//		} else {
//			// Nothing selected.
//			Dialogs.create()
//		        .title("No Selection")
//		        .masthead("No Source Selected")
//		        .message("Please select a source in the table.")
//		        .showWarning();
//		}
	}
	
	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	@FXML
	private void handleNewSource() {
		Source tempSource = new Source("");
		boolean okClicked = mainApp.showSourceEditDialog(tempSource);
		if (okClicked) {
			mainApp.getSourceData().add(tempSource);
		}
	}

	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit
	 * details for the selected person.
	 */
	@FXML
	private void handleEditSource() {
//		Source selectedSource = sourceTable.getSelectionModel().getSelectedItem();
//		if (selectedSource != null) {
//			boolean okClicked = mainApp.showSourceEditDialog(selectedSource);
//			if (okClicked) {
//				showSourceDetails(selectedSource);
//			}
//
////		} else {
////			// Nothing selected.
////			Dialogs.create()
////				.title("No Selection")
////				.masthead("No Source Selected")
////				.message("Please select a source in the table.")
////				.showWarning();
//		}
	}


	public List<String> getConnectionList(NetworkItem networkItem){
		Set<String> connectionList = new TreeSet<>();
		for(String s : networkItem.getSourceListGUID()){
			for(Source source: mainApp.getSourceData()) {
				if(!source.getConnectionGuid().isEmpty()) {
					connectionList.addAll(source.getConnectionGuid().keySet());
				}
			}
		}
//		if(connectionList.isEmpty()){
//			connectionList.add("");
//		}
		return connectionList.isEmpty() ? new ArrayList<>() : new ArrayList<>(connectionList);
	}



	public class Bridge {
		public void exit() {
			Platform.exit();
		}


		public void addNewNetwItem(String name, String ip){
			NetworkItem networkItem = new NetworkItem(ip, name);
			mainApp.getNetworkItems().add(networkItem);
			listtr();
		}
		public void listtr() {
			JSObject windowObject = (JSObject)webEngine.executeScript("window");
//			System.out.println(mainApp.getSourceData());
//			System.out.println(mainApp.getSourceData().get(0));
			List<String> str = new ArrayList<>();
			int i = 0;
			for(NetworkItem s : mainApp.getNetworkItems()){
				StringBuilder s1 = new StringBuilder();
				s1.append("{\"guid\":\"" + s.getGuid() + "\", \"name\":\"" + s.getName()
						+ "\", \"ip\":\"" +s.getIp() +"\"}");
				str.add(s1.toString());
				s1 = null;
				i++;
			}
			System.out.println(str);
//			System.out.println(windowObject.call("getNodes", str));
			windowObject.call("setNodes", str);
			str.clear();
			i=0;
//			List<String> str = new ArrayList<>();
			for(NetworkItem n: mainApp.getNetworkItems()){
				for(String s : getConnectionList(n)) {
					String arrow = "";
					boolean next = false;
					String connectionGuid = "";
					for(Source source : mainApp.getSourceData()){
						if(source.getGuid().equals(s)){
							connectionGuid = source.getParent();
							arrow = source.getConnectionGuid().get(s);
//							arrow = source.getConnectionGuid().get(s);
						}
					}

					for(String string : str){
						if(string.contains(n.getGuid()) && string.contains(connectionGuid)){
							next = true;
						}
					}
					if(!next && !connectionGuid.isEmpty()) {
//						continue;

						StringBuilder s1 = new StringBuilder();
						s1.append("{\"from\":\"" + n.getGuid() + "\", \"to\":\"" + connectionGuid + "\", \"arrow\":\"" + arrow + "\"}");
						str.add(s1.toString());
						System.out.println(s1.toString());
					}
				}
				i++;
			}
			System.out.println(str);
//			windowObject.call("getNodes", str, str1);
			windowObject.call("setEdges",str);
			str.clear();
			windowObject.call("draw()");

		}
		public void listSourcesInNetworkItem(String selectedNodes){
			System.out.println("selected Node = " + selectedNodes);
			String ip = "";
			String name = "";
			NetworkItem networkItemTemp = null;
			for(NetworkItem networkItem: mainApp.getNetworkItems()){
				if(networkItem.getGuid().equals(selectedNodes)){
					ip = networkItem.getIp();
					name = networkItem.getName();
					networkItemTemp = networkItem;
					break;
				}
			}

			ipLabel.setText(ip);
			nameLabel.setText(name);
			if(networkItemTemp!=null) {
//				System.out.println("name = " + networkItemTemp.getName() + " guid =" + networkItemTemp.getGuid());
//				System.out.println("list guids: " + networkItemTemp.getSourceListGUID());
				List<Source> tempSourceList = FXCollections.observableList(new ArrayList<Source>());
				List<String> guidListTemp = networkItemTemp.getSourceListGUID();

				System.out.println(guidListTemp);
				for (Source source : mainApp.getSourceData()) {
					for(String guid : guidListTemp){
//						System.out.println("guid = " + source.getGuid()
//							+ " nI guid's = " + guid );
						if(source.getGuid().equals(guid)){
							tempSourceList.add(source);
						}
					}
				}

				tableView.setItems((ObservableList<Source>) tempSourceList);
//				tableView.setItems(networkItemTemp.getSourceListGUID());
//				nameColumn.setCellValueFactory(
				nameColumn.setCellValueFactory( new PropertyValueFactory<Source, String>("name"));
//						cellData -> cellData.getValue().getName());
				System.out.println("tableView = " + tableView.getItems());
			}
		}
	}

}