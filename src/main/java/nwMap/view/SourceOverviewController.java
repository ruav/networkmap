package nwMap.view;

import nwMap.MainApp;
import nwMap.model.Source;
import nwMap.model.SourceType;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.controlsfx.dialog.Dialogs;

public class SourceOverviewController {
    @FXML
    private TableView<Source> sourceTable;
    @FXML
    private TableColumn<Source, String> nameColumn;
//    @FXML
//    private TableColumn<Person, String> lastNameColumn;

    @FXML
    private Label nameLabel;
    @FXML
    private Label ipLabel;
    @FXML
    private Label typeLabel;
    @FXML
    private Label progIDLabel;
    @FXML
    private Label dbNameLabel;
    @FXML
    private Label loginLabel;
	@FXML
    private Label passwordLabel;
	@FXML
	private Label dbNameSource;
	// Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public SourceOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	// Initialize the source table with the two columns.
        nameColumn.setCellValueFactory(
        		cellData -> cellData.getValue().nameProperty());

        // Clear source details.
        showSourceDetails(null);

        // Listen for selection changes and show the source details when changed.
		sourceTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> showSourceDetails(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        sourceTable.setItems(mainApp.getSourceData());
    }
    
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     * 
     * @param source the person or null
     */
    private void showSourceDetails(Source source) {
		if (source != null) {
    		// Fill the labels with info from the person object.
    		nameLabel.setText(source.getName());
			ipLabel.setText(source.getIp());
    		typeLabel.setText(source.getType().toString());
    		progIDLabel.setText(source.getProgId());
    		dbNameLabel.setText(source.getDbName());
    		loginLabel.setText(source.getLogin());
    		passwordLabel.setText(source.getPassword());
			switch ((SourceType) source.getType()){
				case OPC:
					dbNameSource.setText("Queue name");
					break;
				case AMQ:
					dbNameSource.setText("Queue name");
					break;
				case InfluxDB:
					dbNameSource.setText("DB name");
					break;
				case Redis:
					dbNameSource.setText("DB name (empty)");
					break;
			}
    	} else {
    		// source is null, remove all the text.
			nameLabel.setText("");
			ipLabel.setText("");
			typeLabel.setText("");
			progIDLabel.setText("");
			dbNameLabel.setText("");
			loginLabel.setText("");
			passwordLabel.setText("");
    	}
    }

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteSource() {
		int selectedIndex = sourceTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			sourceTable.getItems().remove(selectedIndex);
		} else {
			// Nothing selected.
			Dialogs.create()
		        .title("No Selection")
		        .masthead("No Source Selected")
		        .message("Please select a source in the table.")
		        .showWarning();
		}
	}
	
	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	@FXML
	private void handleNewSource() {
		Source tempSource = new Source("");
		boolean okClicked = mainApp.showSourceEditDialog(tempSource);
		if (okClicked) {
			mainApp.getSourceData().add(tempSource);
		}
	}

	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit
	 * details for the selected person.
	 */
	@FXML
	private void handleEditSource() {
		Source selectedSource = sourceTable.getSelectionModel().getSelectedItem();
		if (selectedSource != null) {
			boolean okClicked = mainApp.showSourceEditDialog(selectedSource);
			if (okClicked) {
				showSourceDetails(selectedSource);
			}

//		} else {
//			// Nothing selected.
//			Dialogs.create()
//				.title("No Selection")
//				.masthead("No Source Selected")
//				.message("Please select a source in the table.")
//				.showWarning();
		}
	}
}