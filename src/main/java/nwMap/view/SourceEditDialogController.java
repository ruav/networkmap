package nwMap.view;

import nwMap.model.Source;
import nwMap.model.SourceType;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Dialog to edit details of a source.
 * 
 * @author Marco Jakob
 */
public class SourceEditDialogController {

    @FXML
    private TextField nameField;
    @FXML
    private TextField ipField;
    @FXML()
//    private TextField typeField;
    private ComboBox typeField;
    @FXML
    private TextField progIDField;
    @FXML
    private TextField dbnameField;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField keyprfixField;


    private Stage dialogStage;
    private Source source;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the source to be edited in the dialog.
     * 
     * @param source
     */
    public void setSource(Source source) {
        this.source = source;

        nameField.setText(source.getName());
        ipField.setText(source.getIp());
//        typeField.setText(source.getType().toString());
        typeField.getItems().addAll(SourceType.values());
        progIDField.setText(source.getProgId());
        dbnameField.setText(source.getDbName());
        loginField.setText(source.getLogin());
        passwordField.setText(source.getPassword());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            source.setName(nameField.getText());
            source.setIp(ipField.getText());
            source.setType(SourceType.valueOf(typeField.getValue().toString()));
            source.setProgId(progIDField.getText());
            source.setDbName(dbnameField.getText());
            source.setLogin(loginField.getText());
            source.setPassword(passwordField.getText());
            source.setKeyPrefix(keyprfixField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";
        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "No valid first name!\n";
        }
        if (ipField.getText() == null || ipField.getText().length() == 0) {
            ipField.setText("");
        }
        if (typeField.getValue() == null || typeField.getValue().toString().isEmpty()) {
            errorMessage += "No valid type source!\n";
        }

        if (progIDField.getText() == null || progIDField.getText().length() == 0) {
            progIDField.setText("");
        }

        if (dbnameField.getText() == null || dbnameField.getText().length() == 0) {
            dbnameField.setText("");
        }

        if (loginField.getText() == null || loginField.getText().length() == 0) {
            loginField.setText("");
        }

        if (passwordField.getText() == null || passwordField.getText().length() == 0) {
            passwordField.setText("");
        }

        if (keyprfixField.getText() == null || keyprfixField.getText().length() == 0) {
            keyprfixField.setText("");
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
//        	Dialogs.create()
//		        .title("Invalid Fields")
//		        .masthead("Please correct invalid fields")
//		        .message(errorMessage)
//		        .showError();
            return false;
        }
    }
}